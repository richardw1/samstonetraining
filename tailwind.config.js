module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'lime-green-lighter': '#CEFA05',
        'lime-green':'#CAD12C',
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
  
}
