export default {
 
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  image: {
    cloudinary: {
      baseURL: 'https://res.cloudinary.com/west4gym-co-uk/image/upload/'
    }
  },


  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'samstonetraining',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    '@nuxt/image',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  
modules: [
  '@nuxtjs/cloudinary',
  '@nuxt/image',
],
cloudinary: {
  //cloudName: process.env.CLOUDNAME,
    cloudName: 'west4gym-co-uk',
    useComponent: true,
},


  generate: {
    dir: 'public',
    fallback: true
  },

  /*
  ** Customize the base url
  */
  router: {
    base: '/'
  },



  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
